#!/usr/bin/perl

use strict;
my $file=@ARGV[0];

my @args=("pdflatex", "--jobname=FMDG2PDF", "\\def\\FMDGtoPDF{force}\\input{$file}");
system(@args);

open(IN,"$file.tex");

my $count = 1;

while(<IN>) {
    s/\%(.*)//;
    if (/\\FMDG\{(.*?)\}/) {
	    print "$1\n";
	    @args =("pdftk","FMDG2PDF.pdf", "cat", $count, "output", "$1.pdf");
	    system(@args);
	    $count++;
	    }
}
close(IN);


